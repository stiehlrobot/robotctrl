from django.shortcuts import render
from django.http import HttpResponse
from .models import Job

# Create your views here.

def job_add(request):

    print("job add function")
    goal = request.GET['name']
    added = Job(job_name=goal)
    added.save()
    job_list = Job.objects.values_list('job_name')
    l = []
    for n in Job.objects.all():
        l.append(n.job_name)
    return HttpResponse("Added job: {} <br> Jobs in queue: {} <br> Job queue: {}".format(goal, len(l), l))

def job_remove(request):
    print("job finish function")
    if Job.objects.count() > 0:
        #return job with lowest id
        oldest = Job.objects.earliest('id')
        name_of_removed = oldest.job_name
        oldest.delete()
        j = []
        for n in Job.objects.all():
            j.append(n.job_name)
        return HttpResponse("Removed job: {} <br> Jobs in queue: {} <br> Job queue: {}".format(name_of_removed, len(j) ,j))
    else:
        return HttpResponse("No jobs in the queue to delete!")

def job_get(request):
    print("job get function")
    if Job.objects.count() > 0:
        #return job with lowest id
        oldest = Job.objects.earliest('id')
        return HttpResponse("{}".format(oldest.job_name))
    else:
        return HttpResponse("No jobs in the queue!")