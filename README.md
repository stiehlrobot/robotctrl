# Robot Ctrl - Job queue server

This is a small django server that provides add, get, remove functionalities for a job queue to be used for queueing jobs for a Omron LD 60 mobile robot.
