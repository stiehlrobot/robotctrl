from django.urls import path, include

import jobs.views as jv

urlpatterns = [
    path('add/', jv.job_add),
    path('remove/', jv.job_remove),
    path('get/', jv.job_get),
]