from django.db import models

class Job(models.Model):

    job_name = models.CharField(max_length=200)
    created_at = models.DateField(auto_now=True)

    '''
    @classmethod
    def create(cls, job_name):
        job = cls(job_name=job_name)
        return book
    '''